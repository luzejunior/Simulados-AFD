Simulador de Automatos Finitos criado por @Luzenildo Junior para a disciplina de Linguagens Formais e autômatos finitos ministrada pelo professor Andrei Formiga do Centro de Informatica da Universidade Federal da Paraiba.
Implementação simples utilizando linguagem JAVA. Para testar apenas compile e rode o arquivo Main.class
Sintam-se livres para modificar, melhorar ou testar esse codigo.

English:

This is an DFA simulator created by @Luzenildo Junior for Formal Languages class by Dr. Andrei Formiga at Computer Science Center of Federal University of Paraiba, Brazil.
This code have a simple implementation using JAVA language. To test, you only have to compile and run Main.class archive.
The entire code was written in Portuguese, but it can be easily translated to english.
If you have some question or want some translate, feel free to ask me.
Feel free to modify, add something and improve this code.